# incremental-metronome-practice

A tempo practice that targets muscle relaxation while playing at high tempos. Inspired by Marthyn the drummer and my aching wrists while recording my first album. Original metronome part is basically an unofficial fork (because I don't know if I can fork from github to gitlab? of this: https://github.com/cwilso/metronome )
